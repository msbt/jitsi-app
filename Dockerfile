FROM cloudron/base:0.10.0
MAINTAINER Authors name <support@cloudron.io>

WORKDIR /app/code

# EDIT THIS! enter your domain here, needs to somehow be asked before installation
ENV YOURDOMAIN=jitsi.yourdomain.com


#ENV JITSIVERSION=3.3.1

EXPOSE 10000/udp
EXPOSE 4443
EXPOSE 5347


# prerequisites
RUN apt-get update && apt-get install -y apt-transport-https nginx-extras default-jre maven

RUN echo "jitsi-videobridge jitsi-videobridge/jvb-hostname string ${YOURDOMAIN}" | debconf-set-selections && \
    echo "jitsi-meet jitsi-meet/cert-choice select Self-signed certificate will be generated" | debconf-set-selections

RUN wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add - && \
    sh -c "echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list" && \
    apt-get update && apt-get install -y jitsi-meet && rm -rf /var/cache/apt /var/lib/apt/lists


# log stuff
RUN rm -rf /var/log/nginx && ln -s /run/nginx/log /var/log/nginx && \
    rm -rf /var/log/jitsi && ln -s /run/jitsi /var/log/jitsi

# rename configs
RUN mv /etc/jitsi/jicofo/config /etc/jitsi/jicofo/config_orig && \
    mv /etc/jitsi/meet/${YOURDOMAIN}-config.js /etc/jitsi/meet/${YOURDOMAIN}-config.js_orig && \
    mv /etc/jitsi/videobridge/config /etc/jitsi/videobridge/config_orig && \
    mv /etc/jitsi/videobridge/sip-communicator.properties /etc/jitsi/videobridge/sip-communicator.properties_orig && \
    mv /etc/prosody/conf.avail/${YOURDOMAIN}.cfg.lua /etc/prosody/conf.avail/${YOURDOMAIN}.cfg.lua_orig && \
    ln -s /app/data/jicofo_config /etc/jitsi/jicofo/config && \
    ln -s /app/data/meet_config /etc/jitsi/meet/${YOURDOMAIN}-config.js && \
    ln -s /app/data/videobridge_config /etc/jitsi/videobridge/config && \
    ln -s /app/data/sip_config /etc/jitsi/videobridge/sip-communicator.properties && \
    ln -s /app/data/prosody_config /etc/prosody/conf.avail/${YOURDOMAIN}.cfg.lua

# trying to fix permission issues
RUN mv /usr/share/jitsi-meet /app/code/jitsi && \
    ln -s /app/data/meet_config /app/code/jitsi/config.js
    #rm -rf /home/cloudron/.sip-communicator && ln -s /app/data/sip /home/cloudron/.sip-communicator

# nginx stuff
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/lib/nginx && ln -s /run/nginx/lib /var/lib/nginx && \
    chown -R www-data.www-data /run/ /app/code /var/log/jitsi && \
    chmod -R 777 /run


ADD start.sh /app/
ADD nginx.conf /etc/nginx/sites-enabled/

# Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf


CMD [ "/app/start.sh" ]
