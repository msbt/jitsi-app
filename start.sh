#!/bin/bash

set -eux

mkdir -p /app/data/jitsi-web /run/nginx/lib /run/nginx/log /run/jitsi /tmp/jitsi /app/data/sip

if [[ ! -f /app/data/jicofo_config ]]; then
    echo "=> Detected first run"

	cp /etc/jitsi/jicofo/config_orig /app/data/jicofo_config
	cp /etc/jitsi/meet/${APP_DOMAIN}-config.js_orig /app/data/meet_config
	cp /etc/jitsi/videobridge/config_orig /app/data/videobridge_config
	cp /etc/jitsi/videobridge/sip-communicator.properties_orig /app/data/sip_config
	cp /etc/prosody/conf.avail/${APP_DOMAIN}.cfg.lua_orig /app/data/prosody_config

	sed -i "s,JICOFO_HOST=.*,JICOFO_HOST=${APP_DOMAIN}," /app/data/jicofo_config
	sed -i "s,JICOFO_HOSTNAME=.*,JICOFO_HOSTNAME=${APP_DOMAIN}," /app/data/jicofo_config
	sed -i "s,JICOFO_AUTH_DOMAIN=.*,JICOFO_AUTH_DOMAIN=auth.${APP_DOMAIN}," /app/data/jicofo_config

	sed -i "s,domain: .*,domain: '${APP_DOMAIN}'\,," /app/data/meet_config
	sed -i "s,muc: .*,muc: 'conference.${APP_DOMAIN}'," /app/data/meet_config
	sed -i "s,bosh: .*,bosh: '//${APP_DOMAIN}/http-bind'\,," /app/data/meet_config

	sed -i "s,JVB_HOSTNAME=.*,JVB_HOSTNAME=${APP_DOMAIN}\,," /app/data/videobridge_config
        sed -i "s,JVB_HOST=.*,JVB_HOST=${APP_DOMAIN}\,," /app/data/videobridge_config

	cp -R /usr/share/jitsi-meet-orig/* /app/data/jitsi-web/

fi

chown -R www-data.www-data /app/data /run/


echo "Starting server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Jitsi

#exec /usr/sbin/nginx -g 'daemon off;'
